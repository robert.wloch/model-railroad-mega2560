# Ardunio Mega2560 used as model railway controller

## About

That public repository contains my custom Fritzing parts, Fritzing project and source code for controlling my model railway.
The parts that I control or that are involved in the project are PIKO 55290, PIKO 55271, PIKO 55262, Evemodel JTD01-5-EU, Adafruit 16 Channel 12-bit PWM Servo Driver and the eletechsup DC 5-24V 3-5A Flip-Flop Latch DPDT Relay Module Bistable Self-Locking Double Switch Board (DR25E01).

![A breadboard circuit showing the wiring of the Ardunio Mega2560 R3, PIKO 55290, PIKO 55271, PIKO 55262, Evemodel JTD01-5-EU, Adafruit 16 Channel 12-bit PWM Servo Driver and the eletechsup DC 5-24V 3-5A Flip-Flop Latch DPDT Relay Module Bistable Self-Locking Double Switch Board](https://gitlab.com/robert.wloch/model-railroad-mega2560/raw/master/doc/PikoSwitchEvemodelSignal_Steckplatine.png "Breadboard circuit with all components")

## Disclaimer

The project is ongoing and the circuit is under constant review and adaption. What you are seeing is *work in progress* and it comes without any warranties.

## License

All assets in that repository are published under the terms of the [MIT License](https://opensource.org/licenses/MIT):

Copyright 2021 Robert Wloch

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
