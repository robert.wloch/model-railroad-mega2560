/*
Copyright 2021 Robert Wloch

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to 
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies 
of the Software, and to permit persons to whom the Software is furnished to do 
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.
*/
#include <ModelRailwaySwitch.h>

// first PWM driver uses the default address 0x40
Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();
// further PWM drivers need to be addressed like that:
//Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver(0x41);

const int SWITCH_COUNT = 1;
// TwoWaySwitch s1{"S1", pwm, STATE_RIGHT, new int[2]{2, 3}, new int[3]{0, 1, 2}};
TwoWaySwitch s1{"S1", pwm, STATE_RIGHT, new int[2]{2, 3}, new int[2]{0, 1}};

IModelRailwaySwitch *modelRailwaySwitches[1] = {&s1};

void setup()
{
  Serial.begin(9600);
  Serial.println("ModelRailwayMega2560 setup!");

  pwm.begin();
  pwm.setPWMFreq(1000);
  Wire.setClock(200000);

  for (int s = 0; s < SWITCH_COUNT; s++)
  {
    modelRailwaySwitches[s]->setup();
  }
}

void loop()
{
  for (int s = 0; s < SWITCH_COUNT; s++)
  {
    modelRailwaySwitches[s]->loop();
  }
}
