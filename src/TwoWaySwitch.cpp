/*
Copyright 2021 Robert Wloch

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to 
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies 
of the Software, and to permit persons to whom the Software is furnished to do 
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.
*/

#include <ModelRailwaySwitch.h>

TwoWaySwitch::TwoWaySwitch(
    const String name,
    const Adafruit_PWMServoDriver pwm,
    const int initialState,
    const int inputPins[2],
    const int pwmPins[2])
    : m_name{name}, m_pwm{pwm}, m_state{initialState}, m_ready{false}
{
  this->m_inputPins = inputPins;
  this->m_pwmPins = pwmPins;
}

void TwoWaySwitch::setup()
{
  m_ready = false;
  if (sizeof(m_inputPins) != 2 && m_inputPins[0] < 2 && m_inputPins[1] < 2)
  {
    Serial.print("Switch ");
    Serial.print(name());
    Serial.println(" requires two input pins!");
    return;
  }
  if (sizeof(m_pwmPins) != 2 && m_pwmPins[0] < 0 && m_pwmPins[1] < 0)
  {
    Serial.print("Switch ");
    Serial.print(name());
    Serial.println(" requires two PWM pins!");
    return;
  }

  pinMode(m_inputPins[0], INPUT_PULLUP);
  Serial.print("Switch ");
  Serial.print(name());
  Serial.print(" uses input pin ");
  Serial.print(m_inputPins[0]);
  Serial.print(" for green.");

  pinMode(m_inputPins[1], INPUT_PULLUP);
  Serial.print(" Switch ");
  Serial.print(name());
  Serial.print(" uses input pin ");
  Serial.print(m_inputPins[1]);
  Serial.print(" for red.");

  Serial.print(" Switch ");
  Serial.print(name());
  Serial.print(" setup with default state: ");
  Serial.println(m_state == STATE_RIGHT ? "RIGHT." : "LEFT.");

  m_ready = true;

  SyncSwitchSignalsAndTracksToState();
}

void TwoWaySwitch::loop()
{
  if (!m_ready)
  {
    return;
  }

  boolean inputGreen = !digitalRead(m_inputPins[0]);
  boolean inputRed = !digitalRead(m_inputPins[1]);

  if (!(inputGreen || inputRed))
  {
    return;
  }

  int newState = inputGreen ? STATE_RIGHT : STATE_LEFT;
  if (newState != m_state)
  {
    Serial.print("Switch ");
    Serial.print(name());
    Serial.println(" has new state.");
    m_state = newState;
    SyncSwitchSignalsAndTracksToState();
  }
}

void TwoWaySwitch::SyncSwitchSignalsAndTracksToState()
{
  if (m_state == STATE_RIGHT)
  {
    R();
  }
  else
  {
    L();
  }
}

void TwoWaySwitch::L()
{
  if (!m_ready)
  {
    return;
  }

  Serial.print("Switch ");
  Serial.print(name());
  Serial.print(" switches to LEFT: ");

  m_pwm.setPWM(m_pwmPins[1], 4096, 0); // turns pin fully on -> cause low pulse trigger on SRE-05VDC-SL-2C
  delay(50);
  m_pwm.setPWM(m_pwmPins[1], 0, 4096); // release low pulse trigger on SRE-05VDC-SL-2C
  // m_pwm.setPWM(m_pwmPins[2], 0, 4096); // turns pin fully off -> SRD-05VDC-SL-C is on
  // m_pwm.setPWM(m_pwmPins[1], 4096, 0); // turns pin fully on -> SRD-05VDC-SL-C is off
  // delay(50);
  // m_pwm.setPWM(m_pwmPins[1], 0, 4096); // turns pin fully off -> SRD-05VDC-SL-C is on
  Serial.print("Switch");

  m_pwm.setPWM(m_pwmPins[0], 4096, 0); // turns pin fully on -> cause low pulse trigger on SRE-05VDC-SL-2C
  delay(25);
  m_pwm.setPWM(m_pwmPins[0], 0, 4096); // turns pin fully off -> release low pulse trigger on SRE-05VDC-SL-2C
  Serial.println(", signals and track.");
}

void TwoWaySwitch::R()
{
  if (!m_ready)
  {
    return;
  }

  Serial.print("Switch ");
  Serial.print(name());
  Serial.print(" switches to RIGHT: ");

  m_pwm.setPWM(m_pwmPins[1], 4096, 0); // turns pin fully on -> cause low pulse trigger on SRE-05VDC-SL-2C
  delay(50);
  m_pwm.setPWM(m_pwmPins[1], 0, 4096); // turns pin fully off -> release low pulse trigger on SRE-05VDC-SL-2C
  // m_pwm.setPWM(m_pwmPins[1], 0, 4096); // turns pin fully off -> SRD-05VDC-SL-C is on
  // m_pwm.setPWM(m_pwmPins[2], 4096, 0); // turns pin fully on -> SRD-05VDC-SL-C is off
  // delay(50);
  // m_pwm.setPWM(m_pwmPins[2], 0, 4096); // turns pin fully off -> SRD-05VDC-SL-C is on
  Serial.print("Switch");

  m_pwm.setPWM(m_pwmPins[0], 4096, 0); // turns pin fully on -> cause low pulse trigger on SRE-05VDC-SL-2C
  delay(25);
  m_pwm.setPWM(m_pwmPins[0], 0, 4096); // turns pin fully off -> release low pulse trigger on SRE-05VDC-SL-2C
  Serial.println(", signals and track.");
}

String TwoWaySwitch::name()
{
  return m_name;
}
