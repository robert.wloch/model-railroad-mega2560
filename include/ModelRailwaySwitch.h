#ifndef SwitchingStrategies_h
#define SwitchingStrategies_h

#include <Arduino.h>
#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>

const int STATE_LEFT = 0;
const int STATE_STRAIGHT = 1;
const int STATE_RIGHT = 2;

class IModelRailwaySwitch
{
public:
  virtual void setup() = 0;
  virtual void loop();
  virtual String name() = 0;
};

class TwoWaySwitch : public IModelRailwaySwitch
{
public:
  TwoWaySwitch(
      const String name,
      const Adafruit_PWMServoDriver pwm,
      const int initialState,
      const int inputPins[2] = new int[2]{-1, -1},
      const int pwmPins[2] = new int[2]{-1, -1}
      // const int pwmPins[3] = new int[3]{-1, -1, -1}
  );
  virtual void setup();
  virtual void loop();
  virtual String name();

protected:
  virtual void SyncSwitchSignalsAndTracksToState();
  virtual void L();
  virtual void R();

private:
  const String m_name;
  Adafruit_PWMServoDriver m_pwm;
  const int *m_inputPins;
  const int *m_pwmPins;
  int m_state;
  boolean m_ready;
};

#endif